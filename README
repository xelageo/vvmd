Visual Voicemail Daemon (vvmd)
****************************
Copyright (C) 2021, Chris Talbot <chris@talbothome.com>

About
===========================
vvmd is a lower level daemon that retrieves Visual Voicemail

Notes
===========================
The protocols "cvvm", "vvm3", and AT&T USA (a proprietary protocol) configuration works.

References:
Andoird Notes on Visual Voicemail:
https://source.android.com/devices/tech/config/voicemail

Misc Notes on Voicemail:
https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues/253

Visual Voicemail OTMP Specification:
https://www.gsma.com/newsroom/wp-content/uploads/2012/07/OMTP_VVM_Specification_1_3.pdf

Carrier Configuration settings:
https://cs.android.com/android/platform/superproject/+/master:packages/apps/CarrierConfig/assets/

Android Dialer Voicemail Source code:
https://cs.android.com/android/platform/superproject/+/master:packages/apps/Dialer/java/com/android/voicemail/

Modem Manager Settings Notes
===========================
The Modem Manager settings file is in "$HOME/.vvm/$PLUGIN/vvm"

There are several settings in there that are managed by vvmd. Do not attempt to
change them! I am listing below the settings you can change:

It will then attempt to look up your carrier settings from:
https://gitlab.gnome.org/GNOME/mobile-broadband-provider-info

If it cannot find it, I highly suggest for you to add your carrrier settings here.

IMPORTANT NOTE: If you change settings in this file, vvmd MUST NOT BE RUNNING
                in order for your settings to take effect! This is because vvmd
                will overwrite the file when it exits, destroying your changes.

VVMEnabled
	Whether you want to enable Visual Voicemail. Set to "true" or "false"
	Changing this setting via dbus will automatically subscribe/unsubscribe
	vvmd to/from the carrier's visual voicemail.

VVMType
	What type of Visual Voicemail this is. In CarrierConfig, this is
	found under "vvm_type_string".

	AT&T USA is "AT&TUSAProprietary".

	If "vvm_type_string" is not present, use "otmp".

VVMDestinationNumber
	The destination number to send control SMS messages. In CarrierConfig,
	this is	found under "vvm_destination_number_string"

	I am unsure of how to get the destination number if
	"vvm_destination_number_string" is not present.

CarrierPrefix
	The string prefix attached to SMS control messages. I believe Verizon
	Wireless USA changes this to "//VZWVVM", but I have not tested this (nor
	do I remember where I saw this).

	If you use AT&T USA, put your phonenumber without space here (e.g. if
	your phone number is "(432)555-0110", use "4325550110" (without quotes).

	If unsure, leave this to the default "//VVM".

DefaultModemNumber
	If you have multiple modems, this will allow you to only choose which
	modem to connect to by reading its number. The number MUST be set to
	E.164 Setting this to NULL means vvmd will connect to the first modem
	it finds.

	Unless you have multiple modems (not common), you can leave this set to
	"NULL"

Compiling vvmd
============================
In order to compile proxy daemon you need following software packages:
	- GCC compiler
	- D-Bus library
	- GLib library
	- Modem Manager Library
	- Curl Library

Installing vvmd
============================

Build tools requirements

When building and testing directly from the repository it is important to
have at least meson version 0.56 or later installed. In addition, some
supported Meson backend is required (e.g. ninja).

To prepare/configure the project for compilation:

    # meson _build

To build the project:

    # meson compile -C _build

Run tests:

    # meson test -C _build

Install:

    # meson install -C _build


Running from within the source code repository
==============================================

Meson will build the binary in builddir (_build, using the examples above), and
can be invoked directly.

  Run with debugging
    # ./_build/vvmd -d
