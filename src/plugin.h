/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2010-2011, Intel Corporation
 *                2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

struct vvm_plugin_desc {
	const char *name;
	int (*init) (void);
	void (*exit) (void);
};

#ifdef VVM_PLUGIN_BUILTIN
#define VVM_PLUGIN_DEFINE(name, init, exit) \
		struct vvm_plugin_desc __vvm_builtin_ ## name = { \
			#name, init, exit \
		};
#else
#define VVM_PLUGIN_DEFINE(name, init, exit) \
		extern struct vvm_plugin_desc vvm_plugin_desc \
				__attribute__ ((visibility("default"))); \
		struct vvm_plugin_desc vvm_plugin_desc = { \
			#name, init, exit \
		};
#endif
