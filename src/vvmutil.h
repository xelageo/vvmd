/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

//Refer to Android's Dialer, OmtpCvvmMessageSender.java
// dt = device type, 6 = no VTT (transcription) support
#define CCVM_SUFFIX		"dt=6"

#define ACTIVATE_SMS_PREFIX				"Activate:"
#define DEACTIVATE_SMS_PREFIX				"Deactivate:"
#define STATUS_SMS_PREFIX				"STATUS:"
#define PROTOCOL_VERSION_PREFIX				"pv="
#define CLIENT_TYPE_PREFIX 				"ct="
#define TERMINAL_DESTINATION_PORT_NUMBER_PREFIX 	"pt="

//Protocol version 1.1
#define PROTOCOL_VERSION_1_1 "13"
//Protocol version 1.2
#define PROTOCOL_VERSION_1_2 "13"
//Protocol version 1.3
#define PROTOCOL_VERSION_1_3 "13"


struct sms_control_message {
	char *type;
	int   provision_status;
	int   enable_vvm;
	char *mailbox_hostname;
	char *mailbox_port;
	char *vvm_destination_number;
	char *mailbox_username;
	char *mailbox_password;
	char *vvm_type;
	char *default_number;
	char *carrier_prefix;
	int   sync_status_reason;
	char *uid;
	char *new_mailbox_messages;
	int  mailbox_message_type;
	char *message_sender;
	char *message_date;
	char *message_length;
	char *language;
	char *greeting_length;
	char *voice_signature_length;
	char *TUI_password_length;
	char *mailindex;
	char *activate_url;
};

struct voicemail {
	int  mailbox_message_type;
	int  lifetime_status;
	unsigned int message_registration_id;
	char *file_uuid;
	char *email_filepath;
	char *uid;
	char *mailindex;
	char *message_sender;
	char *message_date;
	char *to;
	char *mime_version;
	char *message_context;
	char *content_type;
	char *contents;
	char *attachments;
	char *dbus_path;
};

// Refer to "OMTP_VVM_Specification v1.3, Section 2.8
enum vvmd_provision_status {
	VVM_PROVISION_STATUS_NOT_SET,		//vvmd only, not a VVM Specification
	VVM_PROVISION_STATUS_NEW,
	VVM_PROVISION_STATUS_READY,
	VVM_PROVISION_STATUS_PROVISIONED,
	VVM_PROVISION_STATUS_UNKNOWN,
	VVM_PROVISION_STATUS_BLOCKED,
};

enum vvmd_lifetime_status {
	VVM_LIFETIME_STATUS_UNKNOWN,
	VVM_LIFETIME_STATUS_NOT_READ,
	VVM_LIFETIME_STATUS_READ
};

enum sync_sms_status_reason {
	SYNC_SMS_UNKNOWN,
	SYNC_SMS_NEW_MESSAGE,
	SYNC_SMS_MAILBOX_UPDATE,
	SYNC_SMS_GREETINGS_VOICE_SIGNATURE_UPDATE
};

enum mailbox_message_type {
	MAILBOX_MESSAGE_UNKNOWN,
	MAILBOX_MESSAGE_VOICE,
	MAILBOX_MESSAGE_VIDEO,
	MAILBOX_MESSAGE_FAX,
	MAILBOX_MESSAGE_INFOTAINMENT,
	MAILBOX_MESSAGE_ECC
};

enum SMS_type {
	SMS_MESSAGE_OTHER,
	SMS_MESSAGE_STATUS,
	SMS_MESSAGE_SYNC
};

char *parse_email_address(const char *input);
int vvm_util_parse_sms_message_type(const char *message, const char *carrier_prefix, const char *mailbox_config);
void vvm_util_parse_status_sms_message(const char *message, struct sms_control_message *sms_msg, const char *mailbox_config);
void vvm_util_parse_sync_sms_message(const char *message, struct sms_control_message *sms_msg, const char *mailbox_config);
void vvm_util_delete_status_message(struct sms_control_message *sms_msg);
void vvm_util_delete_vvm_message(struct voicemail *vvm_msg);
char *vvm_util_create_activate_sms(const char *carrier_prefix, const char *vvm_type);
char *vvm_util_create_deactivate_sms(const char *carrier_prefix, const char *vvm_type);
char *vvm_util_create_status_sms(const char *carrier_prefix, const char *vvm_type);
char *vvm_store_generate_uuid_objpath(void);
int vvm_util_decode_vvm_all_email_attachments (struct voicemail *vvm_msg,
					       const char *folderpath);
void vvm_util_decode_vvm_headers (struct voicemail *vvm_msg,
				  char **tokens);
char *decode(const char *input);
