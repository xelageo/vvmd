vvmd storage design
*******************

The vvmd stores the vvm emails on the file system in a directory named
".vvm/<service_identifier>" under the user home directory.
(e.g: /home/<user_name>/.vvm/modemmanager/ -> "modemmanager" is the service
identifier)

A vvm email is stored in a .mbox in a file named with a <UUID>
generated (e.g: 126588fe14db814b781803a17e99b45b0d48.mbox).
Another file with the same prefix, named <UUID>.status
(e.g: 126588fe14db814b781803a17e99b45b0d48.status) contains meta
information related to the email, and any attachments will be stored in a format
"126588fe14db814b781803a17e99b45b0d48$ATTACHMENT"
